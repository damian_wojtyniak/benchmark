﻿using Db4objects.Db4o;
using Library.db4o;
using Library.Interfces;
using Library.Models;
using Library.NHibernate;
using RemoteCourses.Repositories;
using RemoteCourses.Repositories.EntityFramework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Benchmark
{
    class Program
    {
        static IObjectContainer databaseContainer;

        static IUnitOfWork unitOfWork;

        static void Main(string[] args)
        {
            databaseContainer = DatabaseConfiguration.GetDatabaseConnection();

            var unitOfWorkEF = new EFUnitOfWork(new CourseContext());
            unitOfWork = new UnitOfWork(SessionManager.Instance);

            var stopwatch = new Stopwatch();
            var autor = new Author("Damian", "Wojtyniak");
            var autorEF = new Author() { FirstName = "Damian", LastName = "Wojtyniak" };

            var first = 1000;
            var secound = 10000;
            var third = 25000;

            Console.WriteLine(Db4oAddActionTest(stopwatch, first, autor));
            Console.WriteLine(Db4oAddActionTest(stopwatch, secound, autor));
            Console.WriteLine(Db4oAddActionTest(stopwatch, third, autor));
            Console.WriteLine("----------------------------------");
            Console.WriteLine(Db4oUpdateActionTest(stopwatch, first, autor));
            Console.WriteLine(Db4oUpdateActionTest(stopwatch, secound, autor));
            Console.WriteLine(Db4oUpdateActionTest(stopwatch, third, autor));
            Console.WriteLine("----------------------------------");
            Console.WriteLine(Db4oRemoveActionTest(stopwatch, first, autor));
            Console.WriteLine(Db4oRemoveActionTest(stopwatch, secound, autor));
            Console.WriteLine(Db4oRemoveActionTest(stopwatch, third, autor));
            Console.WriteLine("----------------------------------");
            Console.WriteLine("");
            Console.WriteLine(EFAddActionTest(stopwatch, first, unitOfWorkEF, autorEF));
            Console.WriteLine(EFAddActionTest(stopwatch, secound, unitOfWorkEF, autorEF));
            Console.WriteLine(EFAddActionTest(stopwatch, third, unitOfWorkEF, autorEF));
            Console.WriteLine("----------------------------------");
            Console.WriteLine(EFUpdateActionTest(stopwatch, first, unitOfWorkEF));
            Console.WriteLine(EFUpdateActionTest(stopwatch, secound, unitOfWorkEF));
            Console.WriteLine(EFUpdateActionTest(stopwatch, third, unitOfWorkEF));
            Console.WriteLine("----------------------------------");
            Console.WriteLine(EFRemoveActionTest(stopwatch, first, unitOfWorkEF, autorEF));
            Console.WriteLine(EFRemoveActionTest(stopwatch, secound, unitOfWorkEF, autorEF));
            Console.WriteLine(EFRemoveActionTest(stopwatch, third, unitOfWorkEF, autorEF));
            Console.WriteLine("----------------------------------");
            Console.WriteLine("");
            Console.WriteLine(NHAddActionTest(stopwatch, first, autorEF));
            Console.WriteLine(NHAddActionTest(stopwatch, secound, autorEF));
            Console.WriteLine(NHAddActionTest(stopwatch, third, autorEF));
            Console.WriteLine("----------------------------------");
            Console.WriteLine(NHUpdateActionTest(stopwatch, first, autorEF));
            Console.WriteLine(NHUpdateActionTest(stopwatch, secound, autorEF));
            Console.WriteLine(NHUpdateActionTest(stopwatch, third, autorEF));
            Console.WriteLine("----------------------------------");
            Console.WriteLine(NHRemoveActionTest(stopwatch, first, autorEF));
            Console.WriteLine(NHRemoveActionTest(stopwatch, secound, autorEF));
            Console.WriteLine(NHRemoveActionTest(stopwatch, third, autorEF));
            Console.Read();
        }

        private static string Db4oAddActionTest(Stopwatch stopwatch, int repeatAmount, object modelObject)
        {
            stopwatch.Restart();
            stopwatch.Start();

            for (int i = 0; i < repeatAmount; i++)
            {
                databaseContainer.Store(modelObject);
            }

            stopwatch.Stop();

            return $"Db4o stored {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string Db4oUpdateActionTest(Stopwatch stopwatch, int repeatAmount, Author modelObject)
        {
            stopwatch.Restart();
            stopwatch.Start();

            for (int i = 0; i < repeatAmount; i++)
            {
                var author = databaseContainer.Query<Author>(typeof(Author)).FirstOrDefault(x => x.FirstName == modelObject.FirstName);

                author.FirstName = "Zbyszek";

                databaseContainer.Store(author);
            }

            stopwatch.Stop();

            return $"Db4o updated {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string Db4oRemoveActionTest(Stopwatch stopwatch, int repeatAmount, object modelObject)
        {
            stopwatch.Restart();
            stopwatch.Start();

            for (int i = 0; i < repeatAmount; i++)
            {
                databaseContainer.Delete(modelObject);
            }

            stopwatch.Stop();

            return $"Db4o removed {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string EFAddActionTest(Stopwatch stopwatch, int repeatAmount, EFUnitOfWork unitOfWork, Author author)
        {
            stopwatch.Restart();
            stopwatch.Start();

            for (int i = 0; i < repeatAmount; i++)
            {
                unitOfWork.Authors.Add(author);
                unitOfWork.Save();
            }

            stopwatch.Stop();

            return $"Entity Framework added {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string EFUpdateActionTest(Stopwatch stopwatch, int repeatAmount, EFUnitOfWork unitOfWork)
        {
            stopwatch.Restart();
            stopwatch.Start();

            for (int i = 0; i < repeatAmount; i++)
            {
                var updatedAuthor = unitOfWork.Authors.Get(i+1);
                updatedAuthor.FirstName = "Zbyszek";

                unitOfWork.Authors.Update(updatedAuthor);
                unitOfWork.Save();
            }

            stopwatch.Stop();

            return $"Entity Framework updated {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string EFRemoveActionTest(Stopwatch stopwatch, int repeatAmount, EFUnitOfWork unitOfWork, Author author)
        {
            stopwatch.Restart();
            stopwatch.Start();

            //for (int i = 0; i < repeatAmount; i++)
            //{
            //    var allAuthors = unitOfWork.Authors.GetAll();
            //    var delAuthor = allAuthors.FirstOrDefault(x => x.FirstName == author.FirstName);

            //    unitOfWork.Authors.Remove(delAuthor);
            //    unitOfWork.Save();
            //}

            var allAuthors = unitOfWork.Authors.GetAll();
            var authorsAmount = allAuthors.Take(repeatAmount);

            unitOfWork.Authors.RemoveRange(authorsAmount);
            unitOfWork.Save();

            stopwatch.Stop();

            return $"Entity Framework removed {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string NHAddActionTest(Stopwatch stopwatch, int repeatAmount, Author author)
        {
            var authors = new List<Author>();

            for (int i = 0; i < repeatAmount; i++)
            {
                authors.Add(new Author { FirstName = "Damian", LastName = "Wojtyniak" });
            }

            stopwatch.Restart();
            stopwatch.Start();

            unitOfWork.Authors.AddRange(authors);

            stopwatch.Stop();

            return $"Nhibernate added {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string NHUpdateActionTest(Stopwatch stopwatch, int repeatAmount, Author author)
        {
            stopwatch.Restart();
            stopwatch.Start();

            for (int i = 0; i < repeatAmount; i++)
            {
                var newAuthor = unitOfWork.Authors.Get(i + 1);
                newAuthor.FirstName = "Zbyszek";

                unitOfWork.Authors.Update(newAuthor);
            }

            stopwatch.Stop();

            return $"Nhibernate updated {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }

        private static string NHRemoveActionTest(Stopwatch stopwatch, int repeatAmount, Author author)
        {
            stopwatch.Restart();
            stopwatch.Start();

            var allAuthors = unitOfWork.Authors.GetAll();
            var authorsAmount = allAuthors.Take(repeatAmount);

            unitOfWork.Authors.RemoveRange(authorsAmount);

            stopwatch.Stop();

            return $"Nhibernate removed {repeatAmount} records in database in: {stopwatch.ElapsedMilliseconds} miliseconds";
        }
    }
}
