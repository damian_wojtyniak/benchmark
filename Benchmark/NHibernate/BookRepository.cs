﻿using Library.Interfces;
using Library.Models;

namespace Library.NHibernate
{
    class BookRepository : Repository<Book>, IBookRepository
    {
        public BookRepository(SessionManager sessionFactory) : base(sessionFactory) {}
    }
}
