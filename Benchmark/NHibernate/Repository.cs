﻿using Library.Interfces;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Library.NHibernate
{
    class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly SessionManager _sessionFactory;

        protected ISession CurrentSession { get; set; }

        public Repository(SessionManager session)
        {
            _sessionFactory = session;
            CurrentSession = _sessionFactory.CurrentSession();
        }

        public void Add(TEntity entity)
        {
            CurrentSession.Save(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            foreach (TEntity entity in entities)
            {
                CurrentSession.Save(entity);
            }
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return CurrentSession.Query<TEntity>().Where(predicate).ToList();
        }

        public TEntity Get(int id)
        {
            return CurrentSession.Get<TEntity>(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return CurrentSession.Query<TEntity>().ToList();
        }

        public void Remove(TEntity entity)
        {
            CurrentSession.Delete(entity);
            CurrentSession.Flush();
        }

        public void Remove(int id)
        {
            TEntity entity = CurrentSession.Get<TEntity>(id);

            CurrentSession.Delete(entity);
            CurrentSession.Flush();
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            foreach (TEntity entity in entities)
            {
                CurrentSession.Delete(entity);
            }

            CurrentSession.Flush();
        }

        public void Update(TEntity entity)
        {
            CurrentSession.SaveOrUpdate(entity);
            CurrentSession.Flush();
        }
    }
}
