﻿using Library.Interfces;
using Library.Models;

namespace Library.NHibernate
{
    class AuthorRepository : Repository<Author>, IAuthorRepository
    {
        public AuthorRepository(SessionManager sessionFactory) : base(sessionFactory) {}

    }
}
