﻿using Library.Interfces;
using Library.Models;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFReaderRepository : EFRepository<Reader>, IReaderRepository
    {
        public EFReaderRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
