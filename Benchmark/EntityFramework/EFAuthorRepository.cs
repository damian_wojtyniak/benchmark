﻿using Library.Interfces;
using Library.Models;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFAuthorRepository : EFRepository<Author>, IAuthorRepository
    {
        public EFAuthorRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
