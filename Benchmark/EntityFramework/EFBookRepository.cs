﻿using Library.Interfces;
using Library.Models;
using RemoteCourses.Models;

namespace RemoteCourses.Repositories.EntityFramework
{
    class EFBookRepository : EFRepository<Book>, IBookRepository
    {
        public EFBookRepository(CourseContext context) : base(context) { }

        public CourseContext CoursesContext
        {
            get { return Context as CourseContext; }
        }
    }
}
