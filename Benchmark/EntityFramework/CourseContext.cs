﻿using Library.Models;
using System.Data.Entity;

namespace RemoteCourses.Repositories.EntityFramework
{
    class CourseContext : DbContext
    {
        public CourseContext() : base("EFBenchmarkDb")
        {
            // Database.SetInitializer(new CoursesDbInitializer());
        }

        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Reader> Readers { get; set; }
    }
}
