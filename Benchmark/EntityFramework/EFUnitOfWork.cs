﻿using Library.Interfces;
using RemoteCourses.Repositories.EntityFramework;

namespace RemoteCourses.Repositories
{
    class EFUnitOfWork : IUnitOfWork
    {
        private readonly CourseContext _context;
        public IAuthorRepository Authors { get; set; }
        public IBookRepository Books { get; set; }
        public IReaderRepository Readers { get; set; }
        
        public EFUnitOfWork(CourseContext context)
        {
            _context = context;
            Authors = new EFAuthorRepository(_context);
            Books = new EFBookRepository(_context);
            Readers = new EFReaderRepository(_context);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
